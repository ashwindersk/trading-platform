package com.citi.training.trader.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import com.citi.training.trader.dao.TradeMongoRepo;
import com.citi.training.trader.model.Trade;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

/**
 * Unit tests for the TradeService Class. Using Mockito for mocking of dependencies.
 * @see TradeService
 * @see https://site.mockito.org/
 */

@SpringBootTest
public class TradeServiceTests {

    @Autowired
    private TradeService tradeService;

    @MockBean
    private TradeMongoRepo mockRepo;

    @Test
    public void test_TradeService_getAll(){
        // Test List
        List<Trade> results = new ArrayList<Trade>();
        
        when(mockRepo.findAll()).thenReturn(results);

        tradeService.getAll();
        verify(mockRepo, times(1)).findAll();
    }

    @Test
    public void test_TradeService_getAllByTicker(){
        // Test List
        List<Trade> results = new ArrayList<Trade>();
        String testTicker = "TST";
        
        when(mockRepo.findAllByTicker(testTicker)).thenReturn(results);

        tradeService.getAllByTicker(testTicker);
        verify(mockRepo, times(1)).findAllByTicker(testTicker);
    }

    @Test
    public void test_TradeService_getById(){
        // Test Trade
        Trade result = new Trade();
        String testId = "Test#HHHHH";
        
        when(mockRepo.getById(testId)).thenReturn(result);

        tradeService.getById(testId);
        verify(mockRepo, times(1)).getById(testId);
    }
    
    @Test
    public void test_TradeService_cancel(){
        // Test Trade
        Trade result = new Trade();
        String testId = "Test#HHHHH";
        
        when(mockRepo.getById(testId)).thenReturn(result);
        when(mockRepo.save(result)).thenReturn(result);

        tradeService.cancel(testId);
        verify(mockRepo, times(1)).getById(testId);
        verify(mockRepo, times(1)).save(result);
    }

    @Test
    public void test_TradeService_create(){
        // Test Trade
        Trade trade = new Trade();
        
        when(mockRepo.save(trade)).thenReturn(trade);

        tradeService.create(trade);
        verify(mockRepo, times(1)).save(trade);
    }

}
