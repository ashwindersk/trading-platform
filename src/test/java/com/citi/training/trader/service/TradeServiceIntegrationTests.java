package com.citi.training.trader.service;

import java.util.List;

import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.TradeState;
import com.citi.training.trader.model.TradeType;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Integration tests for checking Respository, Service & Model integrations.
 * 
 * @see TradeService
 * @see Trade
 * @see TradeMongoRepo
 */
@SpringBootTest
public class TradeServiceIntegrationTests {

    @Autowired
    private TradeService tradeService;

    @Test
    public void integrationTest_TradeService_getAll(){

        // Set-up
        Trade testTrade1 = new Trade();
        Trade testTrade2 = new Trade();
        Trade testTrade3 = new Trade();

        tradeService.create(testTrade1);
        tradeService.create(testTrade2);
        tradeService.create(testTrade3);

        // Testing
        List<Trade> allTrades = tradeService.getAll();

        // Assert 3 Database entries minimum (Additional May be in Flapdoodle DB)
        assert(allTrades.size() >= 3);
    }

    @Test
    public void integrationTest_TradeService_getAllByTicker(){

        // Set-up
        Trade testTrade1 = new Trade();
        Trade testTrade2 = new Trade();
        Trade testTrade3 = new Trade();

        testTrade1.setTicker("AAA");
        testTrade2.setTicker("BBB");
        testTrade3.setTicker("AAA");

        tradeService.create(testTrade1);
        tradeService.create(testTrade2);
        tradeService.create(testTrade3);

        // Testing
        List<Trade> allTrades = tradeService.getAllByTicker("AAA");

        // Assert 2 Database entries for Ticker
        assert(allTrades.size() == 2);
    }

    @Test
    public void integrationTest_TradeService_getById(){

        // Set-up
        Trade testTrade1 = new Trade();
        Trade testTrade2 = new Trade();
        Trade testTrade3 = new Trade();

        testTrade1.setTicker("TSTA");
        testTrade2.setTicker("TSTB");
        testTrade3.setTicker("TSTC");

        Trade savedTrade1 = tradeService.create(testTrade1);
        tradeService.create(testTrade2);
        tradeService.create(testTrade3);

        // Testing
        Trade trade = tradeService.getById(savedTrade1.getId());

        // Assert correct return.
        assert(trade.getTicker().equals("TSTA"));
    }

    @Test
    public void integrationTest_TradeService_cancel(){

        // Set-up
        Trade testTrade = new Trade();
        Trade savedTrade = tradeService.create(testTrade);

        // Testing
        Trade trade = tradeService.cancel(savedTrade.getId());

        // Assert correct status
        assert(trade.getState() == TradeState.REJECTED);
    }

    @Test
    public void integrationTest_TradeService_create(){

        // Set-up
        Trade testTrade = new Trade();
        testTrade.setPrice(11.11);
        testTrade.setStockQuantity(1111);
        testTrade.setTicker("TSTTCK");
        testTrade.setType(TradeType.BUY);
        
        // Testing
        Trade savedTrade = tradeService.create(testTrade);

        // Assert Correct Parameters
        assert(savedTrade.getPrice() == testTrade.getPrice());
        assert(savedTrade.getStockQuantity() == testTrade.getStockQuantity());

        assert(savedTrade.getType().equals(testTrade.getType()));
        assert(savedTrade.getTicker().equals(testTrade.getTicker()));
    }
}
