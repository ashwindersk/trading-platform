package com.citi.training.trader.rest;

import com.citi.training.trader.service.TradeService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.TradeState;
import com.citi.training.trader.model.TradeType;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import io.swagger.models.Response;

@SpringBootTest
public class TradeControllerTests {

    @Autowired
    private TradeController tradeController;

    @MockBean
    private TradeService mockTradeService;
    private List<Trade> listOfTrades = new ArrayList<>();
     
    
    public TradeControllerTests(){
        Trade trade = new Trade();
        trade.setCreatedDate();
        trade.setPrice(200);
        trade.setStockQuantity(50);
        trade.setTicker("AAPL");
        trade.setType(TradeType.BUY);
        trade.setState(TradeState.CREATED);
        trade.setId("347fs23f94");

        Trade trade2 = new Trade();
        trade2.setCreatedDate();
        trade2.setPrice(20);
        trade2.setStockQuantity(5);
        trade2.setTicker("AAPL");
        trade2.setType(TradeType.BUY);
        trade2.setState(TradeState.CREATED);
        trade2.setId("347fs23f42");

        Trade trade3 = new Trade();
        trade3.setCreatedDate();
        trade3.setPrice(10);
        trade3.setStockQuantity(2);
        trade3.setTicker("AMZN");
        trade3.setType(TradeType.BUY);
        trade3.setState(TradeState.CREATED);
        trade3.setId("347fs");

        listOfTrades.add(trade);
        listOfTrades.add(trade2);
        listOfTrades.add(trade3);
    }

   

    @Test
    public void test_getAll_sanityCheck(){
       
        when(mockTradeService.getAll()).thenReturn(listOfTrades);

        ResponseEntity<List<Trade>> response = tradeController.getAll();
        
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assert(response.getBody().equals(listOfTrades));
    }
    
    @Test
    public void test_getById_sanityCheck(){

        when(this.mockTradeService.getById(listOfTrades.get(0).getId())).thenReturn(listOfTrades.get(0));

        ResponseEntity<Trade> response = tradeController.getById(listOfTrades.get(0).getId());
        
        assert(response.getBody().equals(listOfTrades.get(0)));
    }

    @Test
    public void test_getAllByTicker_sanityCheck(){
        
        when(this.mockTradeService.getAllByTicker(listOfTrades.get(0).getTicker())).thenReturn(listOfTrades);

        ResponseEntity<List<Trade>> response = tradeController.getAllByTicker(listOfTrades.get(0).getTicker());

        //assert(response.status);

    }
}
