package com.citi.training.trader.model;

import java.util.Date;

public class Trade{
    private String id;
    private String ticker;
    private Date createdDate;
    private int stockQuantity;
    private double price;
    private TradeType type = TradeType.BUY;
    private TradeState state = TradeState.CREATED;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTicker() {
        return this.ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    public void setCreatedDate() {
        // SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss z");
        Date createdDate = new Date(System.currentTimeMillis());
        this.createdDate = createdDate;
    }

    public int getStockQuantity() {
        return this.stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public TradeType getType() {
        return type;
    }

    public void setType(TradeType type) {
        this.type = type;
    }

    public TradeState getState() {
        return state;
    }

    public void setState(TradeState state) {
        this.state = state;
    }

}