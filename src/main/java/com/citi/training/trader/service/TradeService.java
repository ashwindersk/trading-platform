package com.citi.training.trader.service;

import java.util.List;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.TradeState;
import com.citi.training.trader.dao.TradeMongoRepo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This class is the main business logic class for the trade request client.
 * Implemented by the TradeController. It is a bean within Spring framework.
 * 
 * @see com.citi.training.trader.controller.TradeController
 * @see com.citi.training.trader.model.TradeRequest
 */

@Component
public class TradeService {

    private final static Logger LOG = LoggerFactory.getLogger(TradeService.class);

    @Autowired
    private TradeMongoRepo tradeMongoRepo;

    /**
     * Method to return all Trades from repository.
     * @return List<Trade>
     */
    public List<Trade> getAll(){
        LOG.debug("All trades returned.");
        return tradeMongoRepo.findAll();
    }

    /**
     * Method to return all Trades from repository by stock Ticker
     * @return List<Trade>
     */
    public List<Trade> getAllByTicker(String ticker){
        LOG.debug("All trades returned for ticker: " + ticker);
        return tradeMongoRepo.findAllByTicker(ticker);
    }

    /**
     * Method to return a Trade from repository by id.
     * @return Trade
     */
    public Trade getById(String id){
        LOG.debug("Trade id: " + id +  " returned.");
        return tradeMongoRepo.getById(id);
    }

    /**
     * Method to cancel a created Trade.
     * @param id
     * @return Trade
     */
    public Trade cancel(String id){
        LOG.debug("Cancelling trade id: " + id);
        Trade trade = tradeMongoRepo.getById(id);
        trade.setState(TradeState.REJECTED);
        return tradeMongoRepo.save(trade);
    }

    /**
     * Method to initialise a new Trade Object, and insert into repository.
     * @param ticker
     * @param orderPrice
     * @param quantity
     * @return Trade
     */
    public Trade create(Trade trade){
        LOG.debug("Trade saved to Database.");
        return tradeMongoRepo.save(trade);
    }
}