package com.citi.training.trader.rest;

import com.citi.training.trader.model.Trade;
import com.citi.training.trader.service.TradeService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;



@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/v1/trade")
public class TradeController{

    private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);

    @Autowired
    private TradeService tradeService;

    @RequestMapping(method=RequestMethod.GET)
    public ResponseEntity<List<Trade>> getAll(){
        LOG.debug("getAll request received");
        return new ResponseEntity<List<Trade>>(tradeService.getAll(), HttpStatus.OK);
    }

    @RequestMapping(path="/{id}", method=RequestMethod.GET)
    public ResponseEntity<Trade> getById(@PathVariable String id){
        LOG.debug("getById request received for id: " + id );
        return new ResponseEntity<Trade>(tradeService.getById(id), HttpStatus.OK);
    }

    @RequestMapping(path="/{ticker}", method=RequestMethod.GET)
    public ResponseEntity<List<Trade>> getAllByTicker(@PathVariable String ticker){
        LOG.debug("getAllByTicker request received");
        return new ResponseEntity<List<Trade>>(tradeService.getAllByTicker(ticker), HttpStatus.OK);
    }

    

    @RequestMapping(method=RequestMethod.POST)
    public ResponseEntity<Trade> create(@RequestBody Trade trade){
        LOG.debug("Create request received");
        return new ResponseEntity<Trade>(tradeService.create(trade), HttpStatus.CREATED);
    } 

    @RequestMapping(path="/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Trade> cancel(@PathVariable String id){
        LOG.debug("Cancel request received for trade id: " + id);
        return new ResponseEntity<Trade>(tradeService.cancel(id), HttpStatus.OK);
    }
}