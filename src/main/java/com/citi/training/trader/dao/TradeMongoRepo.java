package com.citi.training.trader.dao;

import java.util.List;

import com.citi.training.trader.model.Trade;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

/**
 * Data Access Object to connect to Mongo DB.
 * 
 * @see com.citi.training.trader.controller.TradeController
 */

@Component
public interface TradeMongoRepo extends MongoRepository<Trade, String> {
    
    List<Trade> findAllByTicker(String ticker);
    Trade getById(String id);
}
